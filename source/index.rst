.. Scala documentation master file, created by
   sphinx-quickstart on Thu Jun  4 11:44:05 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Scala's documentation!
=================================

Contents:

.. toctree::
    :maxdepth: 1

    getting-started
    tutorials
    cheatsheet
    style-guide
    authors



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

