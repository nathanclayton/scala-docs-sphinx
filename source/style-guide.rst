###########
Style Guide
###########

Contents:

.. toctree::
    :maxdepth: 4

    style-guide/overview
    style-guide/indentation
    style-guide/naming-conventions
    style-guide/types
    style-guide/nested-blocks
    style-guide/declarations
    style-guide/control-structures
    style-guide/method-invocation
    style-guide/files
    style-guide/scaladoc