###############
A Tour of Scala
###############

Contents:

.. toctree::
    :maxdepth: 1

    tour/introduction
    tour/abstract-types
    tour/annotations
    tour/classes
    tour/case-classes
    tour/compound-types
    tour/sequence-comprehensions
    tour/extractor-objects
    tour/generic-classes
    tour/implicit-parameters
    tour/inner-classes
    tour/mixin-class-composition
    tour/nested-functions
    tour/anonymous-function-syntax
    tour/currying
    tour/automatic-type-dependent-closure-construction
    tour/operators
    tour/higher-order-functions
    tour/pattern-matching
    tour/polymorphic-methods
    tour/regular-expression-patterns
    tour/traits
    tour/upper-type-bounds
    tour/lower-type-bounds
    tour/explicitly-typed-self-references
    tour/local-type-inference
    tour/unified-types
    tour/variances
    tour/views
    tour/xml-processing
    tour/default-parameter-values
    tour/named-parameters

