.. toctree::
    :maxdepth: 2

###############
Getting Started
###############

Scala for Programming Beginners
===============================

Your First Lines of Code
========================

The "Hello, world!" Program
---------------------------

Run it Interactively!
---------------------

Compile it!
-----------

Execute it!
-----------

Script it!
----------

