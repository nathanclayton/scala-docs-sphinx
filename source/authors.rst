#######
Authors
#######
*Alphabetical by last name*

=
I
=
 - `Oleg Ilyenko`_ (The awesome icon)

 .. _Oleg Ilyenko: https://github.com/OlegIlyenko/scala-icon

=
O
=

 - `Brendan O'Connor`_ (Cheatsheet)

 .. _Brendan O'Connor: http://brenocon.com/