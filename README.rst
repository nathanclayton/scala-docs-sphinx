###################
Scala Documentation
###################
.. image:: https://readthedocs.org/projects/scala-docs-sphinx/badge/?version=latest
    :target: https://readthedocs.org/projects/scala-docs-sphinx/?badge=latest
    :alt: Documentation Status

This is my attempt to move the Scala documentation from the Jekyll site format that it's currently in
into something that's a bit more documentation-friendly (e.g. able to easily output to PDF and other
alternative formats).

**Please** branch and issue pull requests for any changes you want me to add back in. No change is too
small as long as it improves the documentation, be it the rst files or in the final output.